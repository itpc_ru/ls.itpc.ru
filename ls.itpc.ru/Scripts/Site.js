﻿$('#showLschet').click(function () {
    loadLschet();
    loadpassportwork();
    loadUFMS();
});

$('#city').change(function () {
    $('#house').empty();
    $('#flat').empty();
    var id = $(this).val();
    $.ajax({
        type: 'GET',
        url: '/Autocomplete/GetStreetsByCityId/' + id,
        success: function (data) {
            //$('#street').replaceWith(data);
            $('#street').empty();
            $('#street').append(data);
        }
    });
});


function loadLschet() {
    var id = $('#flat').val();
    $.ajax({
        type: 'GET',
        url: '/Autocomplete/GetLschetByFlatId/' + id,
        success: function (data) {
            $('#lschetHidden').remove();
            $('#lschet').replaceWith(data);
            loadManagementCompany();
            loadServices();
        }
    });
}

function loadpassportwork() {
    var id = $('#house').val();
    $.ajax({
        type: 'GET',
        url: '/Autocomplete/GetPassportWorkByHouseId/' + id,
        success: function (data) {
            $('#PassportWork').replaceWith(data);
        }
    });
}

function loadUFMS() {
    var id = $('#house').val();
    $.ajax({
        type: 'GET',
        url: '/Autocomplete/GetUFMSByHouseId/' + id,
        success: function (data) {
            $('#UFMS').replaceWith(data);
        }
    });
}

function loadManagementCompany() {
    var id = $('#lschetHidden').val();
    $.ajax({
        type: 'GET',
        url: '/Autocomplete/GetManagementCompanyByLschet/' + id,
        success: function (data) {
            $('#ManagementCompany').replaceWith(data);
        }
    });
}

function loadServices() {
    var id = $('#lschetHidden').val();
    $.ajax({
        type: 'GET',
        url: '/Autocomplete/GetServicesForLschet/' + id,
        success: function (data) {
            $('#Services').replaceWith(data);
        }
    });
}