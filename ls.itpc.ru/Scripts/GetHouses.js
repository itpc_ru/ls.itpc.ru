﻿$('#street').change(function () {
    console.log('LOADING HOUSES');
    //$('#house').append(null);
    var id = $(this).val();
    $.ajax({
        type: 'GET',
        url: '@Url.Action("GetHousesByStreetId")/' + id,
        success: function (data) {
            $('#house').replaceWith(data);
        }
    });
});


$('#house').change(function () {
    alert('LOADING FLATS');
    $('#flat').append(null);
    var id = $(this).val();
    $.ajax({
        type: 'GET',
        url: '@Url.Action("GetFlatsByHouseId")/' + id,
        success: function (data) {
            $('#flat').replaceWith(data);
        }
    });
});