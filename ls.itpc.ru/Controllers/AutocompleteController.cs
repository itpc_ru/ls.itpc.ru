﻿using AccountingUnitCheck.data.Storage;
using AccountingUnitCheck.data.Models;
using AccountingUnitCheck.data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;

namespace ls.itpc.ru.Controllers
{
    public class AutocompleteController : Controller
    {
        AddressRepository repo;

        public AutocompleteController()
        {
            repo = new AddressRepository(new AsupContext());
        }

        public ActionResult Index()
        {
            int selectedIndex = 0;

            List<CityModel> cities = repo.GetCities().ToList();
            SelectList selCities = new SelectList(cities, "ID", "City", selectedIndex);
            ViewBag.Cities = selCities;

            List<Street> streets = new List<Street>();
            SelectList selStreets = new SelectList(streets, "ID");
            ViewBag.Streets = selStreets;

            List<House> houses = new List<House>();
            SelectList selHouses = new SelectList(houses, "ID");
            ViewBag.Houses = selHouses;

            List<Flat> flats = new List<Flat>();
            SelectList selFlats = new SelectList(flats, "ID");
            ViewBag.Flats = selFlats;

            return View();
        }

        public ActionResult GetStreetsByCityId(int id)
        {
            IEnumerable<Street> selStreets = repo.GetStreets(id);
            return PartialView(selStreets.OrderBy(x => x.StreetName));
        }

        public ActionResult GetHousesByStreetId(int id)
        {
            IEnumerable<House> selHouses = repo.GetHouses(id);
            return PartialView(selHouses);
        }

        public ActionResult GetFlatsByHouseId(int id)
        {
            IEnumerable<Flat> selFlats = repo.GetFlats(id);
            return PartialView(selFlats);
        }

        public ActionResult GetLschetByFlatId(int id)
        {
            LschetModel selLschet = repo.GetLschet(id);
            return PartialView("GetLschetByFlatId", selLschet);
        }

        public ActionResult GetPassportWorkByHouseId(int id)
        {
            string passportWork = repo.GetPassportWork(id);
            return PartialView("GetPassportWorkByHouseId", passportWork);
        }

        public ActionResult GetUFMSByHouseId(int id)
        {
            string UFMS = repo.GetUFMSByHouseId(id);
            return PartialView("GetUFMSByHouseId", UFMS);
        }

        public ActionResult GetManagementCompanyByLschet(int id)
        {
            string ManagementCompany = repo.GetManagementCompanyByLschet(id);
            return PartialView("GetManagementCompanyByLschet", ManagementCompany);
        }

        public ActionResult GetServicesForLschet(int id)
        {
            string services = repo.GetServicesForLschet(id);
            string[] Services = services.Split(new string[]{","}, StringSplitOptions.RemoveEmptyEntries);
            return PartialView("GetServicesForLschet", Services);
        }
    }
}